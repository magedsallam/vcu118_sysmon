-------------------------------------------------------------------------------
--    
--    Company:          Xilinx
--    Engineer:         Jim Tatsukawa
--    Date:             3/30/2014
--    Design Name:      ug580_tb
--    Module Name:      ug580_tb.v
--    Version:          1.1
--    Target Devices:   UltraScale Architecture
--    Tool versions:    2014.1
--    Description:      This is a basic demonstration of SYSMON
-- 
--    Disclaimer:  XILINX IS PROVIDING THIS DESIGN, CODE, OR
--                 INFORMATION "AS IS" SOLELY FOR USE IN DEVELOPING
--                 PROGRAMS AND SOLUTIONS FOR XILINX DEVICES.  BY
--                 PROVIDING THIS DESIGN, CODE, OR INFORMATION AS
--                 ONE POSSIBLE IMPLEMENTATION OF THIS FEATURE,
--                 APPLICATION OR STANDARD, XILINX IS MAKING NO
--                 REPRESENTATION THAT THIS IMPLEMENTATION IS FREE
--                 FROM ANY CLAIMS OF INFRINGEMENT, AND YOU ARE
--                 RESPONSIBLE FOR OBTAINING ANY RIGHTS YOU MAY
--                 REQUIRE FOR YOUR IMPLEMENTATION.  XILINX
--                 EXPRESSLY DISCLAIMS ANY WARRANTY WHATSOEVER WITH
--                 RESPECT TO THE ADEQUACY OF THE IMPLEMENTATION,
--                 INCLUDING BUT NOT LIMITED TO ANY WARRANTIES OR
--                 REPRESENTATIONS THAT THIS IMPLEMENTATION IS FREE
--                 FROM CLAIMS OF INFRINGEMENT, IMPLIED WARRANTIES
--                 OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
--                 PURPOSE.
 
--                 (c) Copyright 2013-2014 Xilinx, Inc.
--                 All rights reserved.
 
-------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;
use ieee.numeric_std.all;
use ieee.std_logic_textio.all;
library std;
use std.textio.all;
library work;
use work.all;

Library UNISIM;
use UNISIM.VCOMPONENTS.ALL;

entity ug580_tb is
end entity ug580_tb;

architecture tb of ug580_tb is
    signal DCLK               : STD_LOGIC :='0'; 
    signal RESET_n              : STD_LOGIC :='0';
    Signal RESET_n_gen          : STD_logic:= '0';
   
    signal ALM                : STD_LOGIC_VECTOR (15 downto 0);   -- Output data bus for dynamic reconfiguration port
    signal CHANNEL            : STD_LOGIC_VECTOR (5 downto 0);   -- Output data bus for dynamic reconfiguration port
    signal OT                 : STD_LOGIC;   -- Output data bus for dynamic reconfiguration port
    signal EOC         : STD_LOGIC;   -- Output data bus for dynamic reconfiguration port
    signal EOS         : STD_LOGIC;   -- Output data bus for dynamic reconfiguration port
    signal VP                  : STD_LOGIC;
    signal VN                  : STD_LOGIC;
    signal   m_axi_lite_ch1_awaddr :  STD_LOGIC_VECTOR(31 DOWNTO 0);
    signal m_axi_lite_ch1_awprot :  STD_LOGIC_VECTOR(2 DOWNTO 0);
    signal m_axi_lite_ch1_arprot :  STD_LOGIC_VECTOR(2 DOWNTO 0);
    signal m_axi_lite_ch1_awvalid :  STD_LOGIC;
    signal m_axi_lite_ch1_awready : STD_LOGIC;
    signal m_axi_lite_ch1_wdata :  STD_LOGIC_VECTOR(31 DOWNTO 0);
    signal m_axi_lite_ch1_wstrb :  STD_LOGIC_VECTOR(3 DOWNTO 0);
    signal m_axi_lite_ch1_wvalid :  STD_LOGIC;
    signal m_axi_lite_ch1_wready :  STD_LOGIC;
    signal m_axi_lite_ch1_bresp :  STD_LOGIC_VECTOR(1 DOWNTO 0);
    signal m_axi_lite_ch1_bvalid :  STD_LOGIC;
    signal m_axi_lite_ch1_bready :  STD_LOGIC;
    signal m_axi_lite_ch1_araddr :  STD_LOGIC_VECTOR(31 DOWNTO 0);
    signal  m_axi_lite_ch1_arvalid : STD_LOGIC;
    signal m_axi_lite_ch1_arready :  STD_LOGIC;
    signal m_axi_lite_ch1_rdata :  STD_LOGIC_VECTOR(31 DOWNTO 0);
    signal m_axi_lite_ch1_rvalid :  STD_LOGIC;
    signal m_axi_lite_ch1_rresp :  STD_LOGIC_VECTOR(1 DOWNTO 0);
    signal m_axi_lite_ch1_rready :  STD_LOGIC;
    
    signal s_axi_lite_ch1_awaddr :  STD_LOGIC_VECTOR(31 DOWNTO 0);
    signal s_axi_lite_ch1_awprot :  STD_LOGIC_VECTOR(2 DOWNTO 0);
    signal s_axi_lite_ch1_arprot :  STD_LOGIC_VECTOR(2 DOWNTO 0);
    signal s_axi_lite_ch1_awvalid :  STD_LOGIC;
    signal s_axi_lite_ch1_awready : STD_LOGIC;
    signal s_axi_lite_ch1_wdata :  STD_LOGIC_VECTOR(31 DOWNTO 0);
    signal s_axi_lite_ch1_wstrb :  STD_LOGIC_VECTOR(3 DOWNTO 0);
    signal s_axi_lite_ch1_wvalid :  STD_LOGIC;
    signal s_axi_lite_ch1_wready :  STD_LOGIC;
    signal s_axi_lite_ch1_bresp :  STD_LOGIC_VECTOR(1 DOWNTO 0);
    signal s_axi_lite_ch1_bvalid :  STD_LOGIC;
    signal s_axi_lite_ch1_bready :  STD_LOGIC;
    signal s_axi_lite_ch1_araddr :  STD_LOGIC_VECTOR(31 DOWNTO 0);
    signal s_axi_lite_ch1_arvalid : STD_LOGIC;
    signal s_axi_lite_ch1_arready :  STD_LOGIC;
    signal s_axi_lite_ch1_rdata :  STD_LOGIC_VECTOR(31 DOWNTO 0);
    signal s_axi_lite_ch1_rvalid :  STD_LOGIC;
    signal s_axi_lite_ch1_rresp :  STD_LOGIC_VECTOR(1 DOWNTO 0);
    signal s_axi_lite_ch1_rready :  STD_LOGIC;
    
    

begin

   sysmon: entity work.SYSMON_AXI_LITE 
	generic map (
		-- Users to add parameters here

		-- User parameters ends
		-- Do not modify the parameters beyond this line

		-- Width of S_AXI data bus
		C_S_AXI_DATA_WIDTH	=> 32,
		-- Width of S_AXI address bus
		C_S_AXI_ADDR_WIDTH	=> 32
	)
	port map (
		-- Users to add ports here

		-- User ports ends
		-- Do not modify the ports beyond this line

		-- Global Clock Signal
		S_AXI_ACLK	=> dclk,
		-- Global Reset Signal. This Signal is Active LOW
		S_AXI_ARESETN	=> RESET_n,
		-- Write address (issued by master, acceped by Slave)
		S_AXI_AWADDR	=> m_axi_lite_ch1_awaddr,
		-- Write channel Protection type. This signal indicates the
    		-- privilege and security level of the transaction, and whether
    		-- the transaction is a data access or an instruction access.
		S_AXI_AWPROT	=> m_axi_lite_ch1_awprot,
		S_AXI_AWVALID	=> m_axi_lite_ch1_awvalid,
		S_AXI_AWREADY	=> m_axi_lite_ch1_awready,
		S_AXI_WDATA	   => m_axi_lite_ch1_wdata,
		S_AXI_WSTRB	   => m_axi_lite_ch1_wstrb,
		S_AXI_WVALID	=>m_axi_lite_ch1_wvalid,
		S_AXI_WREADY	=>m_axi_lite_ch1_wready,
		S_AXI_BRESP	    => m_axi_lite_ch1_bresp,
		S_AXI_BVALID	=>m_axi_lite_ch1_bvalid,
		S_AXI_BREADY	=>m_axi_lite_ch1_bready,
		S_AXI_ARADDR	 => m_axi_lite_ch1_araddr,
		S_AXI_ARPROT	=> m_axi_lite_ch1_arprot,
		S_AXI_ARVALID	=> m_axi_lite_ch1_arvalid,
		S_AXI_ARREADY	=> m_axi_lite_ch1_arready,
		S_AXI_RDATA	    =>m_axi_lite_ch1_rdata,
		S_AXI_RRESP   	=>  m_axi_lite_ch1_rresp ,
		S_AXI_RVALID	=>  m_axi_lite_ch1_rvalid,
		S_AXI_RREADY	=>m_axi_lite_ch1_rready

	);
	
	      axitraffic: entity work.axi_traffic

	port map (
		  s_axi_aclk          => DCLK,
          s_axi_aresetn       => RESET_n_gen,
    m_axi_lite_ch1_awaddr  => m_axi_lite_ch1_awaddr,
    m_axi_lite_ch1_awprot  =>  m_axi_lite_ch1_awprot,   
    m_axi_lite_ch1_awvalid => m_axi_lite_ch1_awvalid,
    m_axi_lite_ch1_awready => m_axi_lite_ch1_awready,
    m_axi_lite_ch1_wdata   => m_axi_lite_ch1_wdata,
    m_axi_lite_ch1_wstrb   => m_axi_lite_ch1_wstrb,
    m_axi_lite_ch1_wvalid  => m_axi_lite_ch1_wvalid,
    m_axi_lite_ch1_wready  => m_axi_lite_ch1_wready,
    m_axi_lite_ch1_bresp   => m_axi_lite_ch1_bresp,
    m_axi_lite_ch1_bvalid  => m_axi_lite_ch1_bvalid,
    m_axi_lite_ch1_bready  =>  m_axi_lite_ch1_bready ,
    m_axi_lite_ch1_araddr  => m_axi_lite_ch1_araddr,
    m_axi_lite_ch1_arvalid =>  m_axi_lite_ch1_arvalid,
    m_axi_lite_ch1_arready =>  m_axi_lite_ch1_arready,
    m_axi_lite_ch1_rdata   => m_axi_lite_ch1_rdata ,
    m_axi_lite_ch1_rvalid  => m_axi_lite_ch1_rvalid,
    m_axi_lite_ch1_rresp   => m_axi_lite_ch1_rresp ,
    m_axi_lite_ch1_rready  => m_axi_lite_ch1_rready

	);
	


        DCLK <= not DCLK after 10 ns;
      --  RESET <= '1', '0' after 100 ns;
        RESET_n <=  '0' after 10 ns, '1' after 40 ns;
         RESET_n_gen <=  '0' after 10 ns, '1' after 60 us;
      
end architecture tb;
