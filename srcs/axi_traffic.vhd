


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.axi4lite_pkg.all;

entity axi_traffic is
port (
 
    s_axi_aclk : IN STD_LOGIC;
    s_axi_aresetn : IN STD_LOGIC;
    m_axi_lite_ch1_awaddr : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    m_axi_lite_ch1_awprot : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    m_axi_lite_ch1_awvalid : OUT STD_LOGIC;
    m_axi_lite_ch1_awready : IN STD_LOGIC;
    m_axi_lite_ch1_wdata : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    m_axi_lite_ch1_wstrb : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    m_axi_lite_ch1_wvalid : OUT STD_LOGIC;
    m_axi_lite_ch1_wready : IN STD_LOGIC;
    m_axi_lite_ch1_bresp : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    m_axi_lite_ch1_bvalid : IN STD_LOGIC;
    m_axi_lite_ch1_bready : OUT STD_LOGIC;
    m_axi_lite_ch1_araddr : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    m_axi_lite_ch1_arvalid : OUT STD_LOGIC;
    m_axi_lite_ch1_arready : IN STD_LOGIC;
    m_axi_lite_ch1_rdata : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    m_axi_lite_ch1_rvalid : IN STD_LOGIC;
    m_axi_lite_ch1_rresp : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    m_axi_lite_ch1_rready : OUT STD_LOGIC

);
end axi_traffic;

architecture Behavioral of axi_traffic is

-- used to simulate arrival of slow control commands from python-world
-- This is the first traffic gen to wake up the master and slave
COMPONENT axi_traffic_gen_ringv0
  PORT (
    s_axi_aclk : IN STD_LOGIC;
    s_axi_aresetn : IN STD_LOGIC;
    m_axi_lite_ch1_awaddr : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    m_axi_lite_ch1_awprot : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    m_axi_lite_ch1_awvalid : OUT STD_LOGIC;
    m_axi_lite_ch1_awready : IN STD_LOGIC;
    m_axi_lite_ch1_wdata : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    m_axi_lite_ch1_wstrb : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    m_axi_lite_ch1_wvalid : OUT STD_LOGIC;
    m_axi_lite_ch1_wready : IN STD_LOGIC;
    m_axi_lite_ch1_bresp : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    m_axi_lite_ch1_bvalid : IN STD_LOGIC;
    m_axi_lite_ch1_bready : OUT STD_LOGIC;
    m_axi_lite_ch1_araddr : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    m_axi_lite_ch1_arvalid : OUT STD_LOGIC;
    m_axi_lite_ch1_arready : IN STD_LOGIC;
    m_axi_lite_ch1_rdata : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    m_axi_lite_ch1_rvalid : IN STD_LOGIC;
    m_axi_lite_ch1_rresp : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    m_axi_lite_ch1_rready : OUT STD_LOGIC;
    done : OUT STD_LOGIC;
    status : OUT STD_LOGIC_VECTOR(31 DOWNTO 0)
  );
END COMPONENT;



 -- interconnect to funnel the traffic generator outputs to the BE master


signal bid1: STD_LOGIC_VECTOR(0 DOWNTO 0);
signal bid0:  STD_LOGIC_VECTOR(0 DOWNTO 0);
signal m_rid : std_logic_vector(3 downto 0);
signal m_bid : std_logic_vector(3 downto 0);



   
begin

     axi_traffic_gen_ringv0_1_inst : axi_traffic_gen_ringv0
  PORT MAP (
    s_axi_aclk => s_axi_aclk,
    s_axi_aresetn => s_axi_aresetn,
    m_axi_lite_ch1_awaddr  => m_axi_lite_ch1_awaddr,
    m_axi_lite_ch1_awprot  => m_axi_lite_ch1_awprot,   
    m_axi_lite_ch1_awvalid => m_axi_lite_ch1_awvalid,
    m_axi_lite_ch1_awready => m_axi_lite_ch1_awready,
    m_axi_lite_ch1_wdata   => m_axi_lite_ch1_wdata,
    m_axi_lite_ch1_wstrb   => m_axi_lite_ch1_wstrb,
    m_axi_lite_ch1_wvalid  => m_axi_lite_ch1_wvalid,
    m_axi_lite_ch1_wready  => m_axi_lite_ch1_wready,
    m_axi_lite_ch1_bresp   => m_axi_lite_ch1_bresp,
    m_axi_lite_ch1_bvalid  =>  m_axi_lite_ch1_bvalid,
    m_axi_lite_ch1_bready  => m_axi_lite_ch1_bready,
    m_axi_lite_ch1_araddr  => m_axi_lite_ch1_araddr,
    m_axi_lite_ch1_arvalid => m_axi_lite_ch1_arvalid,
    m_axi_lite_ch1_arready => m_axi_lite_ch1_arready,
    m_axi_lite_ch1_rdata   => m_axi_lite_ch1_rdata,
    m_axi_lite_ch1_rvalid  => m_axi_lite_ch1_rvalid,
    m_axi_lite_ch1_rresp   => m_axi_lite_ch1_rresp,
    m_axi_lite_ch1_rready  => m_axi_lite_ch1_rready
  );
 






end Behavioral;
