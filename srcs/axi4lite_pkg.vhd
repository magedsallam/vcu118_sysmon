
library ieee;
use ieee.std_logic_1164.all;

package axi4lite_pkg is

   constant c_axi4lite_addr_w   : natural := 32;
   constant c_axi4lite_data_w   : natural := 32;
   constant c_axi4lite_stb_w    : natural := c_axi4lite_data_w/8;
   constant c_axi4lite_resp_w   : natural := 2;
   
   type t_axi4lite_miso is record -- master in, slave out
      -- write address stream
      awready   : std_logic;
      -- write stream
      wready    : std_logic;
      -- write response
      bresp     : std_logic_vector(c_axi4lite_resp_w-1 downto 0);
      bvalid    : std_logic;
      -- read address stream
      arready   : std_logic;
      -- read response
      rresp     : std_logic_vector(c_axi4lite_resp_w-1 downto 0);
      rdata     : std_logic_vector(c_axi4lite_data_w-1 downto 0);
      rvalid    : std_logic;
   end record; 

   type t_axi4lite_mosi is record -- master out, slave in
      -- write address stream
      awaddr    : std_logic_vector(c_axi4lite_addr_w-1 downto 0); 
      awvalid   : std_logic;
      -- write stream
      wdata     : std_logic_vector(c_axi4lite_data_w-1 downto 0); 
      wvalid    : std_logic;
      wstrb     : std_logic_vector(c_axi4lite_stb_w-1  downto 0);
      ---- write response
      --wready    : std_logic;
      -- read address stream
      araddr    : std_logic_vector(c_axi4lite_addr_w-1 downto 0); 
      arvalid   : std_logic;
      -- read response
      rready    : std_logic;
      bready    : std_logic;
   end record; 
   
   
   
--   -- multi port array for axi4 records
   type t_axi4lite_miso_arr is array (integer range <>) of t_axi4lite_miso;
   type t_axi4lite_mosi_arr is array (integer range <>) of t_axi4lite_mosi;
   
--   -- blank records
--   constant c_axi4lite_miso_rst : t_axi4lite_miso :=
--    ('0', '0', (others=>'0'),'0', '0', (others=>'0'),(others=>'0'),'0');
    
--   constant c_axi4lite_mosi_rst : t_axi4lite_mosi :=
--    ((others=>'0'),'0', (others=>'0'),'0',(others=>'0'), 
--     (others=>'0'),'0', '0','0');

--   -- response values
--   constant c_axi4lite_resp_okay  :std_logic_vector(c_axi4lite_resp_w-1 downto 0):="00";
--   constant c_axi4lite_resp_exokay:std_logic_vector(c_axi4lite_resp_w-1 downto 0):="01";
--   constant c_axi4lite_resp_slverr:std_logic_vector(c_axi4lite_resp_w-1 downto 0):="10";
--   constant c_axi4lite_resp_decerr:std_logic_vector(c_axi4lite_resp_w-1 downto 0):="11";
  
--   type t_ipb_mosi is record
--      addr : std_logic_vector(c_axi4lite_addr_w-1 downto 0);
--      wdat : std_logic_vector(c_axi4lite_data_w-1 downto 0);
--      wreq : std_logic;
--      rreq : std_logic;
--   end record;
   
--   type t_ipb_miso is record
--      wack : std_logic;    
--      rdat : std_logic_vector(c_axi4lite_data_w-1 downto 0);
--      rack : std_logic;
--   end record;
   
   CONSTANT c_axi4stream_tdata_w : INTEGER := 32;
   CONSTANT c_axi4stream_tid_w   : INTEGER := 3;
   CONSTANT c_axi4stream_tdest_w : INTEGER := 32;   -- TODO added for compatibility with axi lite pkg used in FE
   CONSTANT c_axi4stream_tkeep_w : INTEGER := c_axi4stream_tdata_w/8;
   
   TYPE t_axi4stream_mosi IS RECORD
      tdata : STD_LOGIC_VECTOR(c_axi4stream_tdata_w-1 DOWNTO 0); 
      tkeep : STD_LOGIC_VECTOR(c_axi4stream_tkeep_w-1 DOWNTO 0);
      tid   : STD_LOGIC_VECTOR(c_axi4stream_tid_w-1   DOWNTO 0); 
      tdest : STD_LOGIC_VECTOR(c_axi4stream_tdest_w-1   DOWNTO 0);  -- TODO added for compatibility with axi lite pkg used in FE
      tlast : STD_LOGIC;
      tvalid: STD_LOGIC; 
   END RECORD; 
     
   TYPE t_axi4stream_miso IS RECORD
       tready : STD_LOGIC;
   END RECORD; 
   
   type t_axi4stream_mosi_arr is array(integer range <>) of t_axi4stream_mosi;
   type t_axi4stream_miso_arr is array(integer range <>) of t_axi4stream_miso; 


-- TODO constants below added for compatibility with axi lite pkg used in FE
   constant NULL_T_AXI4STREAM_MISO_C : t_axi4stream_miso := (
    tready => '0'
    );
    
   constant NULL_T_AXI4STREAM_MOSI_C : t_axi4stream_mosi := (
      tdata  => (others => '0'),
      tkeep  => (others => '0'),
      tid    => (others => '0'),
      tdest  => (others => '0'),
      tlast  => '0',
      tvalid => '0'
       );


--   CONSTANT c_axi4stream_sosi_rst : t_axi4stream_sosi := 
--     ((OTHERS => '0'), (OTHERS => '0'), (OTHERS => '0'), '0', '0');
--   CONSTANT c_axi4stream_siso_rst : t_axi4stream_siso := 
--    (tready => '0');
end axi4lite_pkg; 